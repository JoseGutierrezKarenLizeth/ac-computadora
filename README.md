# Mapa Mental Von-Neumann vs Hardvard



```plantuml
@startmindmap
* John Von-Neumann
** Nació
*** 28 diciembre 1903
**** Budapest Hungría
** Muere
*** 08 febrero 1957
**** Washington USA
** Aportaciones
*** Desarrolló un programa
**** Almacenador de datos
*** Propuso
**** Adopción del Bit
***** Medida de memoria de ordenadores 0 y 1.
*** Creó 
**** Modelo de Von-Neuman
***** Como es actualmente.
*** Ayudó en la modificación de
**** Computadora ENIAC
***** Pudiendo funcionar
****** Con una sola unidad de procesamiento
***** Propiedades
****** Único espacio de memoria
****** Contenido accesible por posición
****** Ejecución secuencial de instrucciones
** Arquitectura de computadoras de Neumann
*** Compuesta por
**** CPu (unidad central de proceso)
***** Cerebro de la PC
***** Recibe información
***** Procesa información
***** Devuelve la información
**** Memoria principal
***** Capacidad de almacenar información
**** Buses
***** Permite comunicación
****** Entre bloques funcionales del sistema.
**** Dispositivos
***** Entrada/Salida
****** Intermediarios entre el Usuario/Computadora
@endmindmap
```

## Fuentes de información:
>JOHN VON NEUMANN Y SU ARQUITECTURA. (2021, 16 abril). [Vídeo]. YouTube. https://www.youtube.com/watch?v=Rq_DHhYUjzU

>Code, H. (2020, 3 octubre). John Von Neumann: arquitecto de hardware [Vídeo]. YouTube. https://www.youtube.com/watch?>v=FJD-3RKM-5U&feature=youtu.be

>Code, H. (2020b, octubre 10). Arquitectura de computadora: Harvard y Von Neumann [Vídeo]. YouTube. https://www.youtube.com/>watch?v=uICvvS7_nO4&feature=youtu.be

>Sandoval, B. (2013, 24 enero). Arquitectura de Neumann [Vídeo]. YouTube. https://www.youtube.com/watch?v=QscWZ_rven4&>feature=youtu.be

# Supercomputadoras en México

```plantuml
@startmindmap
* Supercomputadoras en México
** ¿Qué es?
*** Infraestructura diseñada
**** Para procesar
***** Grandes cantidades de información más rápida
** Era de la computación en México
*** Inició en la UNAM
**** Tuvo la primer computadora en Latinoamérica.
**** En 1958
***** Computadora IBM 650
****** Funcionaba con bulbos
****** Uso en investigación científica
****** Requería de mucho
******* Espacio 
******* Energía
** Mediados de los años 80´s
*** Implementación de la computadora
**** En la vida cotidiana
** Supercomputadora CRAY 432
*** 1991 UNAM adquiere
**** Primer supercomputadora
**** En América Latina
** Supercomputadora KamBalam
*** Del año 2011
*** Equivale a 
**** Más de 1300 PC´s de escritorio
** Resolución de problemas
*** UNAM decide utilizar las supercomputadoras
**** Hacer millones de cálculos
**** Investigaciones científicas
** Supercomputadora Miztli
*** Adquirida en la UNAM
*** Capacidad de
**** 8344 procesadores
*** Misión
**** Sustituir
***** Laboratorios tradicionales
**** Representar
***** A través de modelos matemáticos
****** Resolución de investigaciones
*** Cuenta con 58 servidores
*** Realiza anualmente
**** 120 proyectos de investigación
** Supercomputadora Xiuhcoatl
*** Diseñada por 
**** UNAM
**** UAM
**** Cinvestav del IPN
*** Creada en 2012
*** Surge
**** Al crear el proyecto
***** Laboratorio Nacional de Cómputo de Alto Desempeño
****** Características 
******* Nodo Robusto de supercómputo 
******* Delta metropolitana de supercómputo
**** Tiempo de vida de 4 años
**** Crecimiento sistemático en su rendimiento
** Supercomputadora de la BUAP
*** Perteneciente a la Benemérita Universidad Autónoma de Puebla
*** Capacidad de almacenamiento
**** Equivalente a 5000 laptops
*** Ejecuta 2000 millones de operaciones/segundo
*** Equipos de acceso remoto
*** Capacidad para simulaciones
*** Ejemplos de uso
**** Investigación de materiales
**** Astronomía
**** Química
**** Aeronáutica



@endmindmap
```
## Citas 
>La supercomputadora de la Benemérita Universidad Autónoma de Puebla. (2018, 14 junio). [Vídeo]. YouTube. https://www.>youtube.com/watch?v=oOgBBHdbBSA&t=7s

>Llegada de la primera computadora a México y a la UNAM. (2018, 26 octubre). [Vídeo]. YouTube. https://www.youtube.com/>watch?v=Z8iuLr6IL64&t=4s

>Nuestra UNAM: Historia de la computación en México. (2011, 4 febrero). [Vídeo]. YouTube. https://www.youtube.com/watch?>v=-lwXKQEqsvk&t=13s

>Conoce la super computadora de la UNAM Miztli. (2017, 27 julio). [Vídeo]. YouTube. https://www.youtube.com/watch?>v=S9f4k1fOp2M&t=3s

>Xiuhcoatl, la segunda computadora más veloz de Latinoamérica. (2016, 7 mayo). [Vídeo]. YouTube. https://www.youtube.com/>watch?v=5VcUGC-1z0Q&t=9s


[JoseGutierrezKarenLizeth@ GitLab](https://gitlab.com/JoseGutierrezKarenLizeth)



